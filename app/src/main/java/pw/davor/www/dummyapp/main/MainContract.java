package pw.davor.www.dummyapp.main;

/**
 * Created by bnc on 25.10.2017..
 */

public interface MainContract {

    interface View{
        void showToast(String message);
        void navigateToHome();
    }

    interface Presenter{

    }
}
