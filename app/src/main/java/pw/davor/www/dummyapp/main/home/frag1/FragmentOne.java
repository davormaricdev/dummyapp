package pw.davor.www.dummyapp.main.home.frag1;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import pw.davor.www.dummyapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentOne extends Fragment implements FragmentOneContract.View {

    private FragmentOneContract.Presenter mPresenter;
    private FragmentOneContract.Model mModel;

    private TextView mText;
    private ProgressBar mProgress;


    public FragmentOne() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mModel = new FragmentModel();
        mPresenter = new FragmentOnePresenter(this, mModel);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_fragment_one, container, false);

        mText = (TextView) v.findViewById(R.id.frag1_lbl);
        mProgress = (ProgressBar) v.findViewById(R.id.frag1_progress);

        if (savedInstanceState == null) {
            // fresh data?
            mPresenter.getMessage();
            Log.d("TAG", "Fragment One - SavedInstance is null");

        }


        return v;
    }


    @Override
    public void updateText(String text) {
        if (text != null) {
            mText.setText(text);
            mText.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);

        }


    }
}
