package pw.davor.www.dummyapp.main.home.frag1;

import android.os.Handler;

/**
 * Created by bnc on 25.10.2017..
 */

public class FragmentModel implements FragmentOneContract.Model {
    @Override
    public void loadMessage(final MessageResult messageResult) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                messageResult.onResult("Response from db");
            }
        }, 1000);

    }
}
