package pw.davor.www.dummyapp.main.home;

/**
 * Created by bnc on 25.10.2017..
 */

public interface HomeContract {
    interface View{
        void navigateToFrag1();
        void navigateToFrag2();
        void showToast(String message);
    }

    interface Presenter{
        void handleFrag1ButtonClick();
        void handleFrag2ButtonClick();
    }
}
