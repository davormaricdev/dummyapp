package pw.davor.www.dummyapp.main.home;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import pw.davor.www.dummyapp.R;
import pw.davor.www.dummyapp.main.MainContract;
import pw.davor.www.dummyapp.main.home.frag1.FragmentOne;
import pw.davor.www.dummyapp.main.home.frag2.FragmentTwo;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements HomeContract.View, View.OnClickListener {
    private HomeContract.Presenter mPresenter;
    private FragmentManager fragmentManager;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getChildFragmentManager();

        mPresenter = new HomePresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        Button mFrag1 = (Button) v.findViewById(R.id.home_btn1);
        Button mFrag2 = (Button) v.findViewById(R.id.home_btn2);

        mFrag1.setOnClickListener(this);
        mFrag2.setOnClickListener(this);


        if(savedInstanceState == null){
            navigateToFrag1();
        }
        return v;
    }

    @Override
    public void navigateToFrag1() {
        FragmentOne fragmentOne = (FragmentOne) fragmentManager.findFragmentByTag("FRAGMENT_ONE");
        if(fragmentOne == null) {
            fragmentOne = new FragmentOne();
        }
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.home_root, fragmentOne, "FRAGMENT_ONE");
            fragmentTransaction.addToBackStack("FRAGMENT_ONE");
            fragmentTransaction.commit();
        }




    @Override
    public void navigateToFrag2() {
        FragmentTwo fragmentTwo = (FragmentTwo) fragmentManager.findFragmentByTag("FRAGMENT_TWO");
        if(fragmentTwo == null) {
            fragmentTwo = new FragmentTwo();
        }
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.home_root, fragmentTwo, "FRAGMENT_TWO");
            fragmentTransaction.addToBackStack("FRAGMENT_TWO");
            fragmentTransaction.commit();


    }

    @Override
    public void showToast(String message) {
        if (message != null) ((MainContract.View) getActivity()).showToast(message);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home_btn1:
                mPresenter.handleFrag1ButtonClick();
                break;

            case R.id.home_btn2:
                mPresenter.handleFrag2ButtonClick();
                break;
        }
    }
}
