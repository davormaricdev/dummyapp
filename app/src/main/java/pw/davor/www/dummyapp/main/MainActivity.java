package pw.davor.www.dummyapp.main;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import pw.davor.www.dummyapp.R;
import pw.davor.www.dummyapp.main.home.HomeFragment;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getSupportFragmentManager();

        if(savedInstanceState == null){
            Log.d("TAG", "Main Saved Instance is null");
            navigateToHome();
        }


    }

    @Override
    public void showToast(String message) {
        if(message != null) Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void navigateToHome() {
        HomeFragment fragment = (HomeFragment) fragmentManager.findFragmentByTag("HOME_FRAG");
        if(fragment == null) {
            Log.d("TAG", "Main Home Fragment is null");
            fragment = new HomeFragment();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_root, fragment, "HOME_FRAG");
            fragmentTransaction.commit();
        }

    }
}
