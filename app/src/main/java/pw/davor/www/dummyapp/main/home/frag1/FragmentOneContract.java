package pw.davor.www.dummyapp.main.home.frag1;

/**
 * Created by bnc on 25.10.2017..
 */

public interface FragmentOneContract {
    interface View{
        void updateText(String text);
    }

    interface Presenter{
        void getMessage();
    }

    interface Model{
        void loadMessage(MessageResult messageResult);
    }
}
