package pw.davor.www.dummyapp.main.home.frag1;

/**
 * Created by bnc on 25.10.2017..
 */

public class FragmentOnePresenter implements FragmentOneContract.Presenter {

    FragmentOneContract.View mView;
    FragmentOneContract.Model mModel;

    public FragmentOnePresenter(FragmentOneContract.View mView, FragmentOneContract.Model mModel) {
        this.mView = mView;
        this.mModel = mModel;
    }


    @Override
    public void getMessage() {
        mModel.loadMessage(new MessageResult() {
            @Override
            public void onResult(String message) {
                mView.updateText(message);
            }
        });

    }
}
