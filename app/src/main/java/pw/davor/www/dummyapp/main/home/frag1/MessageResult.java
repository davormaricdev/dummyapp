package pw.davor.www.dummyapp.main.home.frag1;

/**
 * Created by bnc on 25.10.2017..
 */

interface MessageResult {

    void onResult(String message);
}
