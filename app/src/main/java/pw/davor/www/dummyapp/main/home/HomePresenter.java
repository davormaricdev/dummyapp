package pw.davor.www.dummyapp.main.home;

/**
 * Created by bnc on 25.10.2017..
 */

public class HomePresenter implements HomeContract.Presenter {
    private HomeContract.View mView;

    public HomePresenter(HomeContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void handleFrag1ButtonClick() {
        mView.navigateToFrag1();

    }

    @Override
    public void handleFrag2ButtonClick() {
        mView.navigateToFrag2();

    }
}
